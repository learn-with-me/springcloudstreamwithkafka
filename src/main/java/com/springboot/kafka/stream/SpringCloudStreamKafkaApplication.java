package com.springboot.kafka.stream;

import com.github.javafaker.Commerce;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class SpringCloudStreamKafkaApplication {

	Logger logger = LoggerFactory.getLogger(SpringCloudStreamKafkaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamKafkaApplication.class, args);
	}

	@Bean
	public Supplier<Message<String>> produceProducts() {
		return () -> {
			final Faker faker = Faker.instance();
			final Commerce commerce = faker.commerce();
			final String code = faker.code().ean8();

			return MessageBuilder.withPayload(commerce.productName())
					.setHeader(KafkaHeaders.MESSAGE_KEY, code.getBytes(StandardCharsets.UTF_8))
					.build();
		};
	}

	@Bean
	public Consumer<Message<String>> consumeProducts() {
		return msg -> {
			final String payload = msg.getPayload();
			logger.info("message is = {}",payload);
		};
	}

}
